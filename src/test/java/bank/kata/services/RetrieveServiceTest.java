package bank.kata.services;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import bank.kata.models.Account;
import bank.kata.models.Client;
import bank.kata.models.Operation;
import bank.kata.models.OperationType;

class RetrieveServiceTest {

	@Test
	void clientRetreiveOneHundredUnitClientHasOneHundredUnitTest() {
		double amount = 100.00;
		double currentBalence=0;
		Operation operation = new Operation(1, LocalDateTime.now(), amount, OperationType.WITHDRAWAL);
		executeDepositOperationWhenCanDoOperation(addClientWithOneHundredUnit(),operation, amount,currentBalence);
	}
	
	@Test
	void clientRetreiveTwoHundredUnitClientHasOneHundredUnitTest() {
		double amount = 200.00;
		double currentBalence= 100;
		Operation operation = new Operation(1, LocalDateTime.now(), amount, OperationType.WITHDRAWAL);
		executeDepositOperationWhenCanNotDoTheOperation(addClientWithOneHundredUnit(), amount,currentBalence);
	}

	@Test
	void clientRetreiveOneUnitWhenClientHaveMoneyTest() {
		double amount = 1;
		double currentBalence= 99;
		Operation operation = new Operation(1, LocalDateTime.now(), amount, OperationType.WITHDRAWAL);
		executeDepositOperationWhenCanDoOperation(addClientWithOneHundredUnit(),operation, amount,currentBalence);
	}
	
	@Test
	void clientRetreiveOneUnitWhenClientOpenNewAccountTest() {
		double amount = 1;
		double currentBalence=0;		
		Operation operation = new Operation(1, LocalDateTime.now(), amount, OperationType.WITHDRAWAL);
		executeDepositOperationWhenCanNotDoTheOperation(addClientWithEmptyAccount(), amount,currentBalence);
	}

	@Test
	void clientRetreiveNegatifAmountWhenClientHaveMoneyTest() {
		double amount = -99;
		double currentBalence = 100;
		executeDepositOperationWhenCanNotDoTheOperation(addClientWithOneHundredUnit(),amount,currentBalence);
	}
	
	// no added operation in this case 
	@Test
	void clientRetreiveZeroAmountWhenClientHaveMoneyTest() {
		double amount = 0;
		double currentBalence = 100;
		executeDepositOperationWhenCanNotDoTheOperation(addClientWithOneHundredUnit(),amount,currentBalence);
	}
	
	@Test
	void clientDepositNegatifAmountWhenClientOpenNewAccountTest() {
		double amount = -99;
		double currentBalence=0;
		executeDepositOperationWhenCanNotDoTheOperation(addClientWithEmptyAccount(),amount,currentBalence);
	}
	
	// no added operation in this case 
	@Test
	void clientDepositZeroAmountWhenClientOpenNewAccountTest() {
		double amount = 0;
		double currentBalence=0;
		executeDepositOperationWhenCanNotDoTheOperation(addClientWithEmptyAccount(),amount,currentBalence);
	}

	private void  executeDepositOperationWhenCanDoOperation(Client client,Operation expected, double amount,double currentBalence) {
		
		List<Operation> operations = client.getAccount().getOperations();
		int operationSizeBefore = operations.size();
		boolean isSaved = retreiveService(client, amount);
		Operation lastOperation = operations.get(operations.size() - 1);

		// Verify that the operation added with the right values
		//should return the negative number operation getAmount
		assertEquals(expected.getAmount(), -lastOperation.getAmount());
		assertEquals(expected.getOperationDate().truncatedTo(ChronoUnit.SECONDS),
				lastOperation.getOperationDate().truncatedTo(ChronoUnit.SECONDS));
		assertEquals(expected.getOperationType(), lastOperation.getOperationType());
		// have more operation added to the client operation list
		assertEquals(operationSizeBefore + 1, operations.size());
		//used to update the account because i calculate the balence in the constructor 
		Account account =new Account(client.getAccount().getAccountId(), operations);
		assertEquals(currentBalence, account.getCurrentBalance());
		assertEquals(isSaved, true);
	}

	private void executeDepositOperationWhenCanNotDoTheOperation(Client client, double amount,double currentBalence) {

		
		List<Operation> operations = client.getAccount().getOperations();
		int operationSizeBefore = operations.size();
		boolean isSaved = retreiveService(client, amount);

		// the same number of operations nothing was insterted
		assertEquals(operationSizeBefore, operations.size());
		//used to update the account because i calculate the balence in the constructor 
		Account account =new Account(client.getAccount().getAccountId(), operations);
		assertEquals(currentBalence, account.getCurrentBalance());
		assertEquals(isSaved, false);
	}

	private Client addClientWithOneHundredUnit() {
		List<Operation> operations = new ArrayList<Operation>();
		//initialize the account with 100 unit
		operations.add( new Operation(1, LocalDateTime.now(), 100, OperationType.DEPOSIT));
		return new Client(1, "client 1 ", new Account(1, operations));
	}
	
	private Client addClientWithEmptyAccount() {
		List<Operation> operations = new ArrayList<Operation>();
		return new Client(1, "client 1 ", new Account(1, operations));
	}

	private boolean retreiveService(Client client, double amount) {
		RetrieveService depositService = new RetrieveService();
		return depositService.retrieveMoney(client, amount);
	}

}
