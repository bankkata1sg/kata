package bank.kata.util;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Test;

import bank.kata.models.Operation;
import bank.kata.models.OperationType;

class TransactionIdTest {

	@Test
	void getTransactionIdWhenOperationsListIsEmpltyTest() {
		generateOperationsList(0,0);
		
	}
	
	@Test
	void getTransactionIdWhenOperationsListHasOneElementTest() {
		generateOperationsList(1,1);		
	}

	@Test
	void getTransactionIdWhenOperationsListHasTenElementsTest() {
		generateOperationsList(10,10);		
	}
	
	@Test
	void getTransactionIdWhenOperationsListHasOneHundredElementsTest() {
		generateOperationsList(100,100);		
	}

	

	private void generateOperationsList(int expected, int oprationNumber) {
		List<Operation> operation = new ArrayList<Operation>();
		
		while (oprationNumber>0) {
			operation.add(generateOperation());
			oprationNumber--;
		}
		int transactionId =TransactionId.getTransactionId(operation);
		assertEquals(expected, transactionId);
	}

	private Operation generateOperation() {
		return new Operation(new Random().nextInt(), LocalDateTime.now(), new Random().nextDouble(), OperationType.DEPOSIT);

	}

}
