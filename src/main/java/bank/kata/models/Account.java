package bank.kata.models;

import java.util.List;

public class Account {

	private final int accountId;
	private final double currentBalance;
	private final List<Operation> operations;

	public Account(int accountId,List<Operation> operations) {
		super();
		this.accountId = accountId;
		this.currentBalance = operations.stream().mapToDouble(operation -> operation.getAmount()).sum();
		this.operations = operations;
	}

	public int getAccountId() {
		return accountId;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public List<Operation> getOperations() {
		return operations;
	}

}
