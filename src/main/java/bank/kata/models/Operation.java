package bank.kata.models;

import java.time.LocalDateTime;

public class Operation {

	private int transationId;
	private LocalDateTime operationDate;
	private double amount;
	private OperationType operationType;

	public Operation(int transationId, LocalDateTime operationDate, double amount, OperationType operationType) {
		super();
		this.transationId = transationId;
		this.operationDate = operationDate;
		this.amount = amount;
		this.operationType = operationType;
	}

	public int getTransationId() {
		return transationId;
	}

	public LocalDateTime getOperationDate() {
		return operationDate;
	}

	public double getAmount() {
		return amount;
	}

	public OperationType getOperationType() {
		return operationType;
	}

}
