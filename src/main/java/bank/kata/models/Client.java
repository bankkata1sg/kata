package bank.kata.models;

public class Client {

	private final int clientId;
	private final String name;
	private final Account account;

	public Client(int clientId, String name, Account account) {
		super();
		this.clientId = clientId;
		this.name = name;
		this.account = account;
	}

	public int getClientId() {
		return clientId;
	}

	public String getName() {
		return name;
	}

	public Account getAccount() {
		return account;
	}

}
