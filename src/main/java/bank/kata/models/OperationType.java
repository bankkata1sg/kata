package bank.kata.models;

public enum OperationType {
	DEPOSIT, WITHDRAWAL
}
