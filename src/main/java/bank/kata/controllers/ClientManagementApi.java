package bank.kata.controllers;

import java.util.ArrayList;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bank.kata.models.Account;
import bank.kata.models.Client;
import bank.kata.models.Operation;
import bank.kata.services.DepositService;
import bank.kata.services.RetrieveService;

/*
 * By HSE
 * in the specific case i used new operator usually i use design pattern like builder 
 * i like to work with immutable object  
 * 
 * 
 * */

@RestController
@RequestMapping("/api/v1")
public class ClientManagementApi {

	private final Client client;
	private final DepositService depositService;
	private final RetrieveService retrieveService;

	public ClientManagementApi(DepositService depositService, RetrieveService retrieveService) {
		super();
		this.depositService = depositService;
		this.retrieveService = retrieveService;
		client = new Client(1, "SG", new Account(1, new ArrayList<Operation>()));
	}

	@PostMapping(path = "/deposit")
	public boolean deposit(@RequestBody String amount) {
		JSONObject amountParcer = new JSONObject(amount); 
		return depositService.saveMoney(client, amountParcer.getDouble("amount"));
	}
	
	

	@PostMapping("/retreive")
	public boolean retreive(@RequestBody String amount) {
		JSONObject amountParcer = new JSONObject(amount); 
		Client c = new Client(client.getClientId(), client.getName(),
				new Account(1, client.getAccount().getOperations()));
		return retrieveService.retrieveMoney(c, amountParcer.getDouble("amount"));
	}
	
	@GetMapping("/account")
	public Client accountStatment() {
		return new Client(client.getClientId(), client.getName(),
				new Account(1, client.getAccount().getOperations()));
	}
	
	
	/*
	 * if you don't have postman or simulator tool you can use your navigator to perform 
	 * the tow operations 
	 * 
	 * */
	
	@GetMapping(path = "/navigator/deposit")
	public boolean depositAsGet(@RequestParam double amount) {		
		return depositService.saveMoney(client, amount);
	}
	
	
	@GetMapping("/navigator/retreive")
	public boolean retreiveAsGet(@RequestParam double amount) {
		Client c = new Client(client.getClientId(), client.getName(),
				new Account(1, client.getAccount().getOperations()));
		return retrieveService.retrieveMoney(c, amount);
	}

	

}
