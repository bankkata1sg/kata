package bank.kata.util;

import java.util.List;

import bank.kata.models.Operation;

public class TransactionId {

	public static int getTransactionId(List<Operation> operation) {

		if (operation == null || operation.isEmpty()) {
			return 0;
		} else {
			return operation.size();
		}

	}

}
