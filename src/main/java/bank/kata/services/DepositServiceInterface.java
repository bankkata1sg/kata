package bank.kata.services;

import bank.kata.models.Client;

public interface DepositServiceInterface {

	public boolean saveMoney(Client client, double amount);

}
