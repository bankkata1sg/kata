package bank.kata.services;

import bank.kata.models.Client;

public interface RetrieveServiceInterface {
	
	public boolean retrieveMoney(Client client, double amount);
}
