package bank.kata.services;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import bank.kata.models.Client;
import bank.kata.models.Operation;
import bank.kata.models.OperationType;
import bank.kata.util.TransactionId;

@Service
public class RetrieveService implements RetrieveServiceInterface {

	@Override
	public boolean retrieveMoney(Client client, double amount) {

		List<Operation> operations;
		double currentBalence = client.getAccount().getCurrentBalance();

		if (currentBalence >= amount && amount > 0) {

			operations = client.getAccount().getOperations();

			Operation operation = new Operation(TransactionId.getTransactionId(operations),
					LocalDateTime.now(),
					-amount, OperationType.WITHDRAWAL);

			operations.add(operation);

			return true;

		}

		return false;
	}

}
